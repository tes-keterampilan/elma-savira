# Aplikasi Landing Page Contact Us

Web aplikasi ini merupakan rancangan salah satu halaman pada website Kominfo, yaitu halaman ”hubungi kami”. Halaman ini memiliki fungsi untuk memberikan informasi kepada publik mengenai kontak yang dapat dihubungi jika terdapat pertanyaan kepada Kominfo. Pada halaman “hubungi kami” disediakan pop up berisi formulir yang dapat diisi oleh pengunjung untuk mengirimkan pertanyaan ke Kominfo.

## Installation & Usage

1. Clone this project to your local machine.
2. Run `pnpm i` to install project's dependencies.
3. Run `pnpm run dev` to run a local server to host this project.
