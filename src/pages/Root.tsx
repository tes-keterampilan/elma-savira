import {
  AppShell,
  Burger,
  Center,
  Divider,
  Group,
  Image,
  Text,
  UnstyledButton,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { NavLink, Outlet } from "react-router-dom";
import logoUrl from "../assets/logo.png";
import classes from "./Root.module.css";

const links = [
  [
    {
      name: "Beranda",
      to: "/",
    },
    {
      name: "Tentang Kami",
      to: "/",
    },
    {
      name: "FAQ",
      to: "https://layanan.kominfo.go.id/faqs",
      target: "_blank",
    },
    {
      name: "Contact US",
      to: "/",
    },
  ],
  [
    {
      name: "Masuk",
      to: "/",
    },
    {
      name: "Daftar",
      to: "/",
    },
  ],
];

const Root = () => {
  const [opened, { toggle }] = useDisclosure();
  return (
    <AppShell
      header={{ height: 90 }}
      navbar={{
        width: 300,
        breakpoint: "sm",
        collapsed: { desktop: true, mobile: !opened },
      }}
      footer={{ height: 60 }}
      padding="md"
    >
      <AppShell.Header bg="blue" c="white">
        <Group h="100%" px="md">
          <Burger
            opened={opened}
            onClick={toggle}
            hiddenFrom="sm"
            size="sm"
            color="white"
          />
          <Group justify="space-between" style={{ flex: 1 }}>
            <UnstyledButton>
              <Image src={logoUrl} />
            </UnstyledButton>
            {links &&
              links.map((section, index) => (
                <Group key={index} ml="xl" gap={0} visibleFrom="sm">
                  {section.map((link) => (
                    <UnstyledButton
                      key={link.name}
                      className={classes.control}
                      component={NavLink}
                      to={link.to}
                      target={link.target}
                    >
                      {link.name}
                    </UnstyledButton>
                  ))}
                </Group>
              ))}
          </Group>
        </Group>
      </AppShell.Header>

      <AppShell.Navbar py="md" px={4}>
        {links &&
          links.map((section, index) => (
            <div key={index}>
              {index !== 0 && <Divider />}
              {section.map((link) => (
                <UnstyledButton
                  key={link.name}
                  className={classes.control}
                  component={NavLink}
                  to={link.to}
                  target={link.target}
                >
                  {link.name}
                </UnstyledButton>
              ))}
            </div>
          ))}
      </AppShell.Navbar>

      <AppShell.Main>
        <Outlet />
      </AppShell.Main>

      <AppShell.Footer bg="gray">
        <Center h="100%">
          <Text c="white" size="xl">
            Terwujudnya Indonesia Maju yang Berdaulat, Mandiri, dan
            Berkepribadian Berlandaskan Gotong Royong
          </Text>
        </Center>
      </AppShell.Footer>
    </AppShell>
  );
};
export default Root;
