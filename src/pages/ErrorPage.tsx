import { Center, Stack, Text, Title } from "@mantine/core";

const ErrorPage = () => {
  return (
    <Center h="100vh">
      <Stack>
        <Title order={1}>Oops!</Title>
        <Text>Sorry, an unexpected error has occured...</Text>
      </Stack>
    </Center>
  );
};
export default ErrorPage;
