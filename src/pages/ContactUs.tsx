import {
  Anchor,
  Button,
  Card,
  Center,
  Container,
  Grid,
  Group,
  Image,
  Modal,
  Stack,
  Text,
  TextInput,
  Textarea,
  Title,
} from "@mantine/core";
import { useDisclosure, useViewportSize } from "@mantine/hooks";
import { Link } from "react-router-dom";
import imageUrl from "../assets/waving_kontak.svg";
import clockUrl from "../assets/clock.svg";
import contacts from "../data/contacts.json";

const ContactUs = () => {
  const [opened, { toggle, close }] = useDisclosure(false);
  const { height } = useViewportSize();

  return (
    <Container fluid>
      <Stack>
        {/* Hero Section */}
        <Center h={height - 182}>
          <Group justify="space-between" align="flex-start">
            <Stack flex={1}>
              <Title order={1}>HUBUNGI KAMI</Title>
              <Text>
                Silakan hubungi kami untuk menemukan solusi seputar layanan
                Kementerian Komunikasi dan Informatika
              </Text>
              <Text>
                Lihat halaman{" "}
                <Anchor
                  to={`https://layanan.kominfo.go.id/faqs`}
                  component={Link}
                  target="_blank"
                >
                  FAQ
                </Anchor>{" "}
                kami untuk pertanyaan paling umum
              </Text>
              <Text>
                Atau gunakan formulir di bawah ini untuk menyampaikan pertanyaan
                ke kami
              </Text>
              <Button w="fit-content" onClick={toggle}>
                Formulir
              </Button>

              <Modal
                opened={opened}
                withCloseButton
                onClose={close}
                size="lg"
                radius="md"
                title="Hubungi kami via email"
                centered
              >
                <Stack>
                  <Grid align="center">
                    <Grid.Col span={3}>Nama</Grid.Col>
                    <Grid.Col span={9}>
                      <TextInput />
                    </Grid.Col>
                    <Grid.Col span={3}>Email</Grid.Col>
                    <Grid.Col span={9}>
                      <TextInput />
                    </Grid.Col>
                    <Grid.Col span={12}>
                      <Text>Apa yang bisa kami bantu</Text>
                      <Textarea />
                    </Grid.Col>
                  </Grid>
                  <Button w="fit-content" ml="auto">
                    Kirim
                  </Button>
                </Stack>
              </Modal>
            </Stack>
            <Image src={imageUrl} h={400} visibleFrom="sm" />
          </Group>
        </Center>

        {/* Bantuan Lain Section */}
        <Center h={height - 182}>
          <Stack style={{ flex: 1 }}>
            <Title order={1}>BUTUH BANTUAN LAIN</Title>
            <Grid>
              {contacts &&
                contacts.map((contact, index) => (
                  <Grid.Col key={index} span={4}>
                    <Card shadow="lg" withBorder>
                      <Card.Section px="md" pt="md">
                        <Title order={4}>{contact.name}</Title>
                        {contact.sub && <Title order={6}>{contact.sub}</Title>}
                      </Card.Section>
                      <Card.Section px="md" my="lg">
                        {contact.info &&
                          contact.info.map((i) => (
                            <Text key={i} size="sm">
                              {i}
                            </Text>
                          ))}
                      </Card.Section>
                      <Card.Section px="md" pb="md">
                        <Group>
                          <Image src={clockUrl} />
                          <Text style={{ flex: 1 }}>{contact.layanan}</Text>
                        </Group>
                      </Card.Section>
                    </Card>
                  </Grid.Col>
                ))}
            </Grid>
          </Stack>
        </Center>
      </Stack>
    </Container>
  );
};
export default ContactUs;
